package com.codility.test;

// you can also use imports, for example:
// import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

class Solution1 {
    public int solution(int X, int[] A) {
        int totalDiff = 0;
        for (int a : A) {
            if (a != X) {
                totalDiff++;
            }
        }
        int leftTotalEqual = 0;
        int leftTotalDiff = 0;
        for (int i = 0; i < A.length; i++) {
            final int rightTotalDiff = totalDiff - leftTotalDiff;
            if (leftTotalEqual == rightTotalDiff) {
                return i;
            }
            final int a = A[i];
            if (a == X) {
                leftTotalEqual++;
            } else {
                leftTotalDiff++;
            }
        }
        return A.length;
    }
}