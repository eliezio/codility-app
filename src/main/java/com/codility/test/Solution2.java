package com.codility.test;

public class Solution2 {
    public int[] solution(int[] A) {
        final int X = fromBaseNeg2(A);
        return new int[]{1, 1, 0, 1};
    }

    private int fromBaseNeg2(int[] A) {
        int w = 1;
        int sum = 0;
        for (int a : A) {
            sum += a * w;
            w *= -2;
        }
        return sum;
    }
}
