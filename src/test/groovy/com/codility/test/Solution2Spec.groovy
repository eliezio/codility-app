package com.codility.test

import spock.lang.Specification
import spock.lang.Unroll

class Solution2Spec extends Specification {
    @Unroll
    "problem2 A=#A"() {
        expect:
        new Solution2().solution(A as int[]) == (R as int[])

        where:
        A                  || R
        [1, 0, 0, 1, 1]    || [1, 1, 0, 1]
        [1, 0, 0, 1, 1, 1] || [1, 1, 0, 1, 0, 1, 1]
    }
}
