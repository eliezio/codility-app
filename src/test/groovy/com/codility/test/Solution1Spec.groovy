package com.codility.test

import spock.lang.Specification
import spock.lang.Unroll

class Solution1Spec extends Specification {
    @Unroll
    "problem1 X=#X, A=#A"() {
        expect:
        new Solution1().solution(X, A as int[]) == K

        where:
        A                     | X || K
        [5, 5, 1, 7, 2, 3, 5] | 5 || 4
    }
}
